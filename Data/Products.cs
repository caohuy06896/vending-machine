using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vending_Machine.Data
{
    public class Products
    {
        [Key]
        public int ID { get; set; }
        public string Details { get; set; }
        public string Name { get; set; }
        public int InStock { get; set; }
        public double UnitPrice { get; set; } 
    }
}
