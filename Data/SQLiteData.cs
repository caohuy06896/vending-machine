using Microsoft.Data.Sqlite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vending_Machine.Data
{
    public class SQLiteData
    {
        private static SqliteConnection sqlite_conn;
        public static SqliteConnection CreateConnection()
        {
            string relativePath = Environment.CurrentDirectory + "VendingMachine.sqlite";
            //string currentPath;
            string absolutePath;
            string connectionString;
            //currentPath = System.Reflection.Assembly.GetExecutingAssembly().Location;
            absolutePath = System.IO.Path.Combine(relativePath);

            connectionString = string.Format("DataSource=" + absolutePath);

            sqlite_conn = new SqliteConnection(connectionString);

            //string currentDirectory = Directory.GetCurrentDirectory();
            //string filePath = System.IO.Path.Combine(currentDirectory, "Data", "Locker");
            //// Create a new database connection:
            //sqlite_conn = new SqliteConnection(@"Data Source= " + filePath);
            // Open the connection:
            try
            {
                sqlite_conn.Open();
            }
            catch (Exception ex)
            {

            }
            return sqlite_conn;
        }
        public static void ReadData()
        {
            SqliteDataReader sqlite_datareader;
            SqliteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "SELECT * FROM Locker";

            sqlite_datareader = sqlite_cmd.ExecuteReader();
            while (sqlite_datareader.Read())
            {
                string myreader = sqlite_datareader.GetString(0);
                Console.WriteLine(myreader);
            }
            sqlite_conn.Close();
        }
    }
}
