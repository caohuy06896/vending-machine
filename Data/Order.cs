using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vending_Machine.Data
{
    public class Order
    {
        [Key]
        public int ID { get; set; }
        public DateTime OrderDate { get; set; }
        public double Price { get; set; }
        public int ProductID { get; set; }
        public int Quantity { get; set; }
        public int CustomerID { get; set; }
    }
}
